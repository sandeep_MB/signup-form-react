import React, { Component } from "react";
import "./About.css";

class About extends React.Component {
  render() {
    return (
      <div className="about-section">
        <div className="about">
          <h1 className="heading">About Us</h1>
          <p className="para">
            The Robin Hood Army is a zero-funds volunteer organization that
            works to get surplus food from restaurants and communities to serve
            the less fortunate. Our “Robins” are largely students and young
            working professionals – everyone does this in their free time. The
            lesser fortunate sections of society we serve include homeless
            families, orphanages, patients from public hospitals and old age
            homes.
          </p>
        </div>
      </div>
    );
  }
}

export default About;
