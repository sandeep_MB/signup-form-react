import React from "react";
import "./SignUp.css";
import validator from "validator";

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      age: "",
      address: "",
      password: "",
      confirmPassword: "",
      checked: false,
      success: false,
      errors: {
        nameError: "",
        ageError: "",
        emailError: "",
        passwordError: "",
        confirmPasswordError: "",
        checkboxError: "",
      },
      message: "",
    };
  }

  handleChange = (e) => {
    if (this.state.message) {
      this.setState({ message: "" });
    }
    const { name, value } = e.target;
    console.log(`${name} : ${value}`);
    this.setState({
      [name]: value,
    });
    return;
  };
  handleForm = (e) => {
    // console.log(e.target[5].checked)
    const { name, value } = e.target;
    let hasError = false;
    e.preventDefault();
    let errors = {
      nameError: "",
      ageError: "",
      emailError: "",
      passwordError: "",
      confirmPasswordError: "",
      checkboxError: "",
      success: false,
    };

    // if (this.state.success === false) {
    if (validator.isEmpty(this.state.name)) {
      errors.nameError = "*Please Enter name";
      hasError = true;
    } else if (!validator.isAlpha(this.state.name)) {
      errors.nameError = "*Name must be letters";
    } else if (!validator.isLength(this.state.name, { min: 2, max: 15 })) {
      errors.nameError = "* Name must be min 2 letters and max 15 letters";
    }
    if (validator.isEmpty(this.state.email)) {
      errors.emailError = "*Please Enter Email Address";
      hasError = true;
    } else if (
      !validator.isEmail(this.state.email, { blacklisted_chars: "#" })
    ) {
      errors.emailError = "*Please enter a valid email";
      hasError = true;
    }
    if (!validator.isInt(this.state.age)) {
      errors.ageError = "*Please Enter valid age";
      hasError = true;
    } else if (!(Number(this.state.age) > 0 && Number(this.state.age) < 120)) {
      errors.ageError = "*Age must be between 1 and 100";
      hasError = true;
    }
    if (validator.isEmpty(this.state.password)) {
      errors.passwordError = "*Enter your password";
      hasError = true;
    } else {
      if (validator.isEmpty(this.state.confirmPassword)) {
        errors.confirmPasswordError = "*Enter password again";
        hasError = true;
      } else if (
        !validator.equals(this.state.password, this.state.confirmPassword)
      ) {
        errors.confirmPasswordError = "*Password Doesn't Matched";
        hasError = true;
      }
    }
    if (!e.target[5].checked) {
      errors.checkboxError = "*Accept terms and conditions";
      hasError = true;
    }
    this.setState((this.state.errors = errors));
    // }
    if (!hasError) {
      e.target[5].checked = false;
      this.setState({
        name: "",
        email: "",
        age: "",
        address: "",
        password: "",
        confirmPassword: "",
        message: "Form Sumbited Successfully",
      });
      console.log(this.state.message);
    }
  };

  // needsValidation = (e) => {
  //   console.log(e.target.checkValidity());
  //   if (e.target.checkValidity()) {
  //     e.target.preventDefault();
  //     e.target.stopPropagation();
  //   }
  //   e.target.classList.add("was-validated");
  // };

  render() {
    return (
      <div className="form-group">
        <div className="form">
          <form
            className=" w-50 form-signUp"
            onSubmit={this.handleForm}
            novalidate
          >
            <div>
              <div className="success">{this.state.message}</div>
              <h1 className="heading">SignUp</h1>
              {/* Name */}
              <div className="mb-3">
                <label for="inputField" className="form-label">
                  Name
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputField"
                  value={this.state.name}
                  type="text"
                  name="name"
                  placeholder="Your name"
                  onChange={this.handleChange}
                />
                <div className="error">{this.state.nameError}</div>
              </div>

              {/* Email */}
              <div className="mb-3">
                <label for="emailField" className="form-label">
                  Email
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="emailField"
                  value={this.state.email}
                  type="text"
                  name="email"
                  placeholder="Your email"
                  onChange={this.handleChange}
                />
                <div className="error">{this.state.emailError}</div>
              </div>

              <div className="mb-3">
                <label for="ageField" className="form-label">
                  Age
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="passwordField"
                  value={this.state.age}
                  type="text"
                  name="age"
                  placeholder="Your age"
                  onChange={this.handleChange}
                />
                <div className="error">{this.state.ageError}</div>
              </div>

              <div className="mb-3">
                <label for="passwordField" className="form-label">
                  Password
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="passwordField"
                  value={this.state.password}
                  type="password"
                  name="password"
                  placeholder="Your password"
                  onChange={this.handleChange}
                />
                <div className="error">{this.state.passwordError}</div>
              </div>

              <div className="mb-3">
                <label for="confirmPasswordField" className="form-label">
                  Confirm Password
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="confirmPasswordField"
                  value={this.state.confirmPassword}
                  type="password"
                  name="confirmPassword"
                  placeholder="Confirm your password"
                  onChange={this.handleChange}
                />
                <div className="error">{this.state.confirmPasswordError}</div>
              </div>

              <div className="check col-12">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="checked"
                    value={this.state.checked}
                    id="invalidCheck"
                  />
                  <label className="form-check-label" for="invalidCheck">
                    Agree to terms and conditions
                  </label>
                  <div className="error">{this.state.checkboxError}</div>
                </div>
              </div>

              <div class="submit-btn col-12">
                <button class="btn btn-primary" type="submit">
                  Submit form
                </button>
              </div>
            </div>
          </form>
          <div className="w-50 image-container">
            <img
              src="https://images.unsplash.com/photo-1573164574472-797cdf4a583a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=869&q=80"
              alt=" corporate image"
            />
          </div>
        </div>
      </div>
    );
  }
}
