import React, { Component } from "react";
import "./Header.css";
import { Link } from "react-router-dom";
class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <ul className="ul-list">
          <Link className="list" to="/">
            Home
          </Link>
          <Link className="list" to="/about">
            About
          </Link>
          <Link className="list" to="/SignUp">
            SignUp
          </Link>
        </ul>
      </header>
    );
  }
}

export default Header;
