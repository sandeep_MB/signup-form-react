import React from "react";
import "./Home.css";

export default class Home extends React.Component {
  render() {
    return (
      <main className="main-section">
        <div className="main">
          <h1>Introduction </h1>
          <p className="para">
            The standard chunk of Lorem Ipsum used since the 1500s is reproduced
            below for those interested. Sections 1.10.32 and 1.10.33 from "de
            Finibus Bonorum et Malorum" by Cicero are also reproduced in their
            exact original form, accompanied by English versions from the 1914
            translation by H. Rackham
          </p>
        </div>
      </main>
    );
  }
}
